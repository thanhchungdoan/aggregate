package com.cgi.Aggregate.controller;

import com.cgi.Aggregate.model.TicketDto;
import com.cgi.Aggregate.service.AggrToServiceNow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
@RequestMapping("/aggr")
public class AggrToServiceNowController {

    AggrToServiceNow aggrToServiceNow;
    RestTemplate restTemplate;

    @Autowired
    public AggrToServiceNowController(AggrToServiceNow aggrToServiceNow, RestTemplate restTemplate) {
        this.aggrToServiceNow = aggrToServiceNow;
        this.restTemplate = restTemplate;
    }

    @RequestMapping("tickets/{idTicket}")
    public List<AggrToServiceNow> getTicket(@PathVariable("idTicket") String idTicket) {
        //get all tickets ID
        TicketDto ticketDto = restTemplate.getForObject("http://localhost:8094/servicenow/api/v1/tickets/" + idTicket
                , TicketDto.class);
        return aggrToServiceNow.getTicketsFromServiceNow();
    }
}
