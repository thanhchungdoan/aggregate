package com.cgi.Aggregate.model;

public class Aggr {

    private Long id;

    public Aggr() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Aggr{" +
                "id=" + id +
                '}';
    }
}
