package com.cgi.Aggregate.model;

import java.time.LocalDate;

public class TicketDto {

    private Long idTicket;
    private String name;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private LocalDate creationDate;
    private LocalDate ticketCloseDate;

    public TicketDto() {
    }

    public Long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getTicketCloseDate() {
        return ticketCloseDate;
    }

    public void setTicketCloseDate(LocalDate ticketCloseDate) {
        this.ticketCloseDate = ticketCloseDate;
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDate=" + creationDate +
                ", ticketCloseDate=" + ticketCloseDate +
                '}';
    }
}
