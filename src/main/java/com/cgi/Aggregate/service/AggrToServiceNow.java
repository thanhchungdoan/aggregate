package com.cgi.Aggregate.service;

import com.cgi.Aggregate.model.TicketDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AggrToServiceNow {

    public List<AggrToServiceNow> getTicketsFromServiceNow() {
        List<TicketDto> ticketDtos = new ArrayList();

        return ticketDtos.stream().map(ticketDto -> new AggrToServiceNow())
                .collect(Collectors.toList());
    }
}
