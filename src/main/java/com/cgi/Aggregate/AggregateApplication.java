package com.cgi.Aggregate;


import com.cgi.Aggregate.model.TicketDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class AggregateApplication {

    private static final String REST_URI = "http://localhost:8094/servicenow/api/v1/tickets";
    private RestTemplate restTemplate = new RestTemplate();

    public static void main(String[] args) {
        SpringApplication.run(AggregateApplication.class, args);
        AggregateApplication agg = new AggregateApplication();


    }

    public TicketDto getTicketDto(int id) {
        HttpEntity<String> entity = new HttpEntity<>();
        ResponseEntity<TicketDto> response = restTemplate.exchange(REST_URI + "/" + id, HttpMethod.GET, entity, TicketDto.class);
        if (response.getStatusCode().isError()) {
            // log user not found

        }
        return response.getBody();
    }


}